//
//  AppDelegate.swift
//  PennyJar
//
//  Created by Rachel Schneebaum on 8/25/17.
//  Copyright © 2017 Rachel Schneebaum. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    // MARK: - lifecycle

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        checkForLoggedInUser()
        return true
    }

    // MARK: - navigation

    func switchRootViewController(_ root: UIViewController, animated: Bool = true, completion: (() -> Void)? = nil) {
        if animated {
            UIView.transition(with: window!, duration: 0.5, options: .transitionCrossDissolve, animations: {
                let oldState = UIView.areAnimationsEnabled
                UIView.setAnimationsEnabled(false)
                self.window!.rootViewController = root
                UIView.setAnimationsEnabled(oldState)
            }, completion: { finished in
                if let completion = completion {
                    completion()
                }
            })
        } else {
            window!.rootViewController = root
        }
    }

    private func proceedWithUser(_ user: User) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let navVC = storyboard.instantiateViewController(
            withIdentifier: IDs.NavigationController.main) as! UINavigationController
        let homeVC = navVC.viewControllers[0] as! HomeViewController
        homeVC.user = user
        switchRootViewController(navVC)

        OAuth2Handler.shared.delegate?.OAuthCanceled()
    }

    private func backToLogin() {
        OAuth2Handler.shared.delegate?.OAuthCanceled()

        if window?.rootViewController is LoginViewController {
            // root vc is login; do nothing
        } else {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let loginVC = storyboard.instantiateViewController(
                withIdentifier: LoginViewController.id) as! LoginViewController
            switchRootViewController(loginVC)
        }
    }

    // MARK: - check/refresh user session

    private func checkForLoggedInUser() {
        if let _ = Keychain.value(forKey: Keys.bearerToken) {
            refresh()
        } else {
            backToLogin()
        }
    }

    private func refresh() {
        if let rootVC = window?.rootViewController as? LoginViewController,
            let spinner = rootVC.spinner {
            spinner.startAnimating()
        }

        AuthService.shared.refresh { result in
            switch result {
            case .success(let user):
                self.proceedWithUser(user)

            case .failure(let error):
                print(error.localizedDescription)
                self.backToLogin()
            }
        }
    }
}

