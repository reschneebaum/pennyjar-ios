//
//  LoginViewController.swift
//  PennyJar
//
//  Created by Rachel Schneebaum on 8/25/17.
//  Copyright © 2017 Rachel Schneebaum. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController {

    @IBOutlet private weak var loginButton: UIButton!
    @IBOutlet weak var spinner: UIActivityIndicatorView!

    override func viewDidLoad() {
        super.viewDidLoad()
        OAuth2Handler.shared.delegate = self
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)

        // check if we're returning from webview
        guard spinner.isAnimating,
            let _ = OAuth2Handler.shared.OAuthToken,
            let user = AuthService.shared.currentUser else {
                return OAuthCanceled()
        }

        OAuthResumed()
        let navVC = storyboard?.instantiateViewController(
            withIdentifier: IDs.NavigationController.main) as! UINavigationController
        let destination = navVC.viewControllers[0] as! HomeViewController
        destination.user = user

        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.switchRootViewController(navVC)

        OAuthCanceled()
    }

    @IBAction func onLoginTapped(_ sender: UIButton) {
        sender.isEnabled = false
        spinner.startAnimating()

        OAuth2Handler.shared.startOAuth2Flow { url in
            let navVC = self.storyboard?.instantiateViewController(
                withIdentifier: IDs.NavigationController.web) as! UINavigationController
            let destination = navVC.viewControllers[0] as! WebViewController
            destination.url = url
            
            self.present(navVC, animated: true, completion: nil)
        }
    }
}

// MARK: - OAuthDelegate
extension LoginViewController: OAuthDelegate {

    func OAuthCanceled() {
        loginButton.isEnabled = true
        spinner.stopAnimating()
        loginButton.setTitle(LocalizedStrings.loginButton, for: .normal)
    }

    func OAuthResumed() {
        loginButton.isEnabled = false
        spinner.startAnimating()
        loginButton.setTitle(LocalizedStrings.loggingIn, for: .normal)
        loginButton.setTitle(LocalizedStrings.loggingIn, for: .disabled)
    }
}
