//
//  OAuth2Handler.swift
//  PennyJar
//
//  Created by Rachel Schneebaum on 9/10/17.
//  Copyright © 2017 Rachel Schneebaum. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

protocol OAuthDelegate {
    func OAuthCanceled()
    func OAuthResumed()
}

class OAuth2Handler {

    static let shared = OAuth2Handler()
    private init() {}

    var delegate: OAuthDelegate?

    var OAuthToken: String? {
        if let token = Keychain.value(forKey: Keys.oAuthCode), !token.isEmpty {
            return token
        } else {
            return nil
        }
    }

    // MARK: - OAuth2 Flow

    func startOAuth2Flow(completion: @escaping (URL) -> Void) {
        AuthService.shared.login { result in
            switch result {
            case .success(let urlString):
                print("url returned: \(urlString)")

                guard let url = URL(string: urlString) else {
                    print("oauth canceled")
                    self.delegate?.OAuthCanceled()
                    return
                }

                if let id = url.queryParameterValue(for: Keys.client_id) {
                    _ = Keychain.set(id, forKey: Keys.clientID)
                }

                completion(url)

            case .failure(let error):
                print("error: \(error.localizedDescription)")
                self.delegate?.OAuthCanceled()
            }
        }
    }

    func processOAuthStepTwo(completion: @escaping (OAuthResult<User>) -> Void) {
        guard let authToken = self.OAuthToken else {
            return completion(.failure(.noOAuthCode))
        }

        AuthService.shared.sendAuthToken(authToken) { result in
            switch result {
            case .success(let user):
                completion(.success(user))

            case .failure(let error):
                guard case APIError.returned(let string) = error else {
                    return completion(.failure(.noBearerToken))
                }
                completion(.failure(.other(string)))
            }
        }
    }
}
