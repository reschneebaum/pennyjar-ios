//
//  WebViewController.swift
//  PennyJar
//
//  Created by Rachel Schneebaum on 1/18/18.
//  Copyright © 2018 Rachel Schneebaum. All rights reserved.
//

import UIKit
import WebKit

class WebViewController: UIViewController {

    var url: URL!

    @IBOutlet private weak var progressView: UIProgressView!
    private var webView = UIWebView()

    override func viewDidLoad() {
        super.viewDidLoad()

        progressView.progress = 0
        webView.frame = view.frame
        view.addSubview(webView)

        webView.delegate = self
        webView.loadRequest(URLRequest(url: url))
    }
}

// MARK: - UIWebViewDelegate
extension WebViewController: UIWebViewDelegate {
    func webViewDidStartLoad(_ webView: UIWebView) {
        UIView.animate(withDuration: 0.5, animations: {
            self.progressView.progress = 0.8
        })
    }

    func webViewDidFinishLoad(_ webView: UIWebView) {
        print(webView.request?.url?.path ?? "")

        UIView.animate(withDuration: 0.2, animations: {
            self.progressView.progress = 1.0
        }, completion: { _ in
            UIView.animate(withDuration: 0.3, animations: {
                self.progressView.alpha = 0
            })
        })

        if let url = webView.request?.url,
            let code = url.queryParameterValue(for: Keys.code) {
            _ = Keychain.set(code, forKey: Keys.oAuthCode)

            OAuth2Handler.shared.processOAuthStepTwo { [weak self] result in
                guard let strongSelf = self else { return }
                switch result {
                case .success(let user):
                    AuthService.shared.currentUser = user
                    strongSelf.dismiss(animated: true, completion: nil)

                case .failure(let error):
                    print("error: \(error.localizedDescription)")
                    strongSelf.dismiss(animated: true, completion: nil)
                }
            }
        }
    }

    func webView(_ webView: UIWebView, didFailLoadWithError error: Error) {
        dismiss(animated: true, completion: nil)
    }
}
