//
//  CampaignDetailViewController.swift
//  PennyJar
//
//  Created by Rachel Schneebaum on 1/24/18.
//  Copyright © 2018 Rachel Schneebaum. All rights reserved.
//

import UIKit

class CampaignDetailViewController: UIViewController {

    var campaign: Campaign!

    @IBOutlet weak var jarImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var progressLabel: UILabel!
    @IBOutlet weak var progressView: UIProgressView!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var avatarImageView: UIImageView!
    @IBOutlet weak var creatorNameLabel: UILabel!
    private var amountTextField: UITextField?

    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
        // TODO: - there's got to be a better way of doing this!
        // when this viewcontroller is loaded, its campaign becomes the stored currentCampaign for the app
        CampaignService.shared.currentCampaign = campaign
    }

    @objc func onDetailsTapped(_ sender: Any) {
        let tabBarController = storyboard?.instantiateViewController(
            withIdentifier: IDs.TabBarController.campaignDetail) as! UITabBarController
        tabBarController.modalTransitionStyle = .crossDissolve
//        navigationController?.show(tabBarController, sender: self)
        present(tabBarController, animated: true, completion: nil)
    }

    @IBAction func onDepositTapped(_ sender: UIButton) {
        let depositAlert = UIAlertController(
            title: LocalizedStrings.contribute,
            message: "You are contributing to the jar \(campaign.title ?? "").\n\nYou will only be charged if the jar meets or exceeds its mimimum goal amount of $\(campaign.goalString).\n\nDo not press 'Contribute to Jar' unless you are prepared to pay the amount entered below or $1.00.",
            preferredStyle: .alert)
        depositAlert.addTextField { [weak self] textField in
            guard let strongSelf = self else { return }
            strongSelf.amountTextField = textField
            textField.placeholder = LocalizedStrings.amount
            textField.keyboardType = .numberPad
        }
        depositAlert.addAction(UIAlertAction(title: LocalizedStrings.contributeToJar, style: .default) { _ in
            self.contributeToJar()
        })
        depositAlert.addAction(UIAlertAction(title: "Cancel", style: .cancel) { _ in
            self.amountTextField = nil
        })
        present(depositAlert, animated: true, completion: nil)
    }

    func contributeToJar() {
        guard let amountText = amountTextField?.text else { return }
        CampaignService.shared.addContribution(to: campaign, amount: amountText) { [weak self] result in
            guard let strongSelf = self else { return }
            switch result {
            case .success(let campaign):
                strongSelf.campaign = campaign
                strongSelf.updateCampaignDetails(for: campaign)

            case .failure(let error):
                print(error.localizedDescription)
            }
        }
    }
}

// MARK: - private
private extension CampaignDetailViewController {
    func setup() {
        guard let campaign = campaign else { return }
        title = campaign.title ?? LocalizedStrings.jarDetails
        setupNavBarButton()

        updateCampaignDetails(for: campaign)
    }

    func setupNavBarButton() {
        let button = UIButton(type: .custom)
        button.setTitle(LocalizedStrings.info, for: .normal)
        button.setTitleColor(view.tintColor, for: .normal)
        button.addTarget(self, action: #selector(onDetailsTapped(_:)), for: .touchUpInside)
        let barButton = UIBarButtonItem(customView: button)
        navigationItem.rightBarButtonItem = barButton
    }

    func updateCampaignDetails(for campaign: Campaign) {
        titleLabel.text = campaign.title ?? ""
        progressLabel.text = "$\(campaign.currentAmountString) of $\(campaign.goalString)"
        progressView.progress = Float(campaign.currentAmount / campaign.goal)
        descriptionLabel.text = campaign.description ?? ""

        if let firstName = campaign.creator?.firstName {
            let lastName = campaign.creator?.lastName ?? ""
            creatorNameLabel.text = "\(firstName) \(lastName)"

            if let avatar = campaign.creator?.avatar {
                avatarImageView.image = avatar
            }
        }

        if let urlString = campaign.imageURLString,
            let url = URL(string: urlString) {
            do {
                let data = try Data(contentsOf: url)
                if let image = UIImage(data: data) {
                    jarImageView.image = image
                }

            } catch {
                print("error setting image = \(error)")
            }
        }
    }
}
