//
//  CampaignStore.swift
//  PennyJar
//
//  Created by Rachel Schneebaum on 1/24/18.
//  Copyright © 2018 Rachel Schneebaum. All rights reserved.
//

import UIKit

protocol CampaignStore {
    var campaigns: [Campaign] { get set }
    var type: CampaignService.ReturnedType { get }
    func retrieveCampaigns(completion: @escaping ([Campaign]) -> Void)
    func sectionHeader() -> String
}

extension CampaignStore {
    func retrieveCampaigns(completion: @escaping ([Campaign]) -> Void) {
        CampaignService.shared.getCampaigns(type: type) { result in
            guard case .success(let campaigns) = result else {
                return completion([])
            }
            completion(campaigns)
        }
    }
}

class MyCampaignsStore: NSObject, CampaignStore {
    var campaigns: [Campaign] = []
    let type: CampaignService.ReturnedType = .user

    func sectionHeader() -> String {
        return LocalizedStrings.myCampaigns
    }
}

extension MyCampaignsStore: UICollectionViewDataSource, UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return campaigns.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(
            withReuseIdentifier: CampaignCollectionViewCell.id,
            for: indexPath) as! CampaignCollectionViewCell

        let campaign = campaigns[indexPath.row]

        cell.setImage(nil, orURLString: campaign.imageURLString)
        return cell
    }
}

class OtherCampaignsStore: NSObject, CampaignStore {
    var campaigns: [Campaign] = []
    let type: CampaignService.ReturnedType = .other

    func sectionHeader() -> String {
        return LocalizedStrings.otherCampaigns
    }
}

extension OtherCampaignsStore: UICollectionViewDataSource, UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return campaigns.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(
            withReuseIdentifier: CampaignCollectionViewCell.id,
            for: indexPath) as! CampaignCollectionViewCell

        let campaign = campaigns[indexPath.row]

        cell.setImage(nil, orURLString: campaign.imageURLString)
        return cell
    }
}

class ClosedCampaignsStore: NSObject, CampaignStore {
    var campaigns: [Campaign] = []
    let type: CampaignService.ReturnedType = .closed

    func sectionHeader() -> String {
        return LocalizedStrings.closedCampaigns
    }
}

extension ClosedCampaignsStore: UICollectionViewDataSource, UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return campaigns.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(
            withReuseIdentifier: CampaignCollectionViewCell.id,
            for: indexPath) as! CampaignCollectionViewCell

        let campaign = campaigns[indexPath.row]

        cell.setImage(nil, orURLString: campaign.imageURLString)
        return cell
    }
}
