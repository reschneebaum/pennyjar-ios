//
//  ContributionsViewController.swift
//  PennyJar
//
//  Created by Rachel Schneebaum on 1/24/18.
//  Copyright © 2018 Rachel Schneebaum. All rights reserved.
//

import UIKit

class ContributionsViewController: UIViewController {
    var contributions: [Contribution] = []

    @IBOutlet private weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
        getContributions()
    }

    func getContributions() {
        guard let campaign = CampaignService.shared.currentCampaign else { return }
        CampaignService.shared.getContributions(for: campaign) { [weak self] result in
            guard let strongSelf = self else { return }
            guard case .success(let contributions) = result else { return }
            strongSelf.contributions = contributions
            if contributions.count > 0 {
                strongSelf.tableView.separatorStyle = .singleLine
            }
            strongSelf.tableView.reloadData()
        }
    }

    @objc func back() {
        dismiss(animated: true, completion: nil)
    }
}

// MARK: - UITableViewDataSource
extension ContributionsViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return contributions.count == 0 ? 1 : contributions.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: ContributionTableViewCell.id,
                                                 for: indexPath) as! ContributionTableViewCell
        if contributions.count > 0 {
            cell.setup(contribution: contributions[indexPath.row])
        } else {
            cell.setupForEmpty()
        }

        return cell
    }
}

// MARK: - UITableViewDelegate
extension ContributionsViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
}

// MARK: - private
private extension ContributionsViewController {
    func setup() {
        title = LocalizedStrings.contributions
        let backButton = UIBarButtonItem(title: LocalizedStrings.back,
                                         style: .plain,
                                         target: self,
                                         action: #selector(back))
        navigationItem.leftBarButtonItem = backButton
        setupTableView()
    }

    func setupTableView() {
        tableView.tableFooterView = UIView(frame: CGRect.zero)
        tableView.register(UINib(nibName: ContributionTableViewCell.id, bundle: nil),
                           forCellReuseIdentifier: ContributionTableViewCell.id)
        tableView.separatorStyle = .none
    }
}
