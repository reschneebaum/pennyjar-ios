//
//  HomeViewController.swift
//  PennyJar
//
//  Created by Rachel Schneebaum on 9/10/17.
//  Copyright © 2017 Rachel Schneebaum. All rights reserved.
//

import UIKit

class HomeViewController: UIViewController {
    var user: User!
    var campaignStores: [CampaignStore] = []

    @IBOutlet private weak var tableView: UITableView!

    private var storedOffsets: [Int: CGFloat] = [:]
    private let interactor = Interactor()

    override func viewDidLoad() {
        super.viewDidLoad()
        title = LocalizedStrings.title
        tableView.register(UINib(nibName: CollectionViewTableViewCell.id, bundle: nil),
                           forCellReuseIdentifier: CollectionViewTableViewCell.id)
        tableView.tableFooterView = UIView(frame: CGRect.zero)

        fetchCampaigns()
    }

    @IBAction func openMenu(_ sender: UIBarButtonItem) {
        performSegue(withIdentifier: IDs.Segue.menu, sender: nil)
    }

    @IBAction func edgePanGesture(_ sender: UIScreenEdgePanGestureRecognizer) {
        let translation = sender.translation(in: view)
        let progress = MenuService.calculateProgress(translationInView: translation,
                                                     viewBounds: view.bounds,
                                                     direction: .right)
        MenuService.mapGestureStateToInteractor(gestureState: sender.state,
                                                progress: progress,
                                                interactor: interactor) {
            self.performSegue(withIdentifier: IDs.Segue.menu, sender: nil)
        }
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let destinationVC = segue.destination as? MenuViewController {
            destinationVC.transitioningDelegate = self
            destinationVC.interactor = interactor
            destinationVC.delegate = self
        }
    }

    private func fetchCampaigns() {
        let myCampaignStore = MyCampaignsStore()
        myCampaignStore.retrieveCampaigns { campaigns in
            myCampaignStore.campaigns = campaigns
            self.tableView.reloadSections([0], with: .fade)
        }

        let otherCampaignStore = OtherCampaignsStore()
        otherCampaignStore.retrieveCampaigns { campaigns in
            otherCampaignStore.campaigns = campaigns
            self.tableView.reloadSections([1], with: .fade)
        }

        let closedCampaignStore = ClosedCampaignsStore()
        closedCampaignStore.retrieveCampaigns { campaigns in
            closedCampaignStore.campaigns = campaigns
            self.tableView.reloadSections([2], with: .fade)
        }

        campaignStores = [myCampaignStore, otherCampaignStore, closedCampaignStore]
    }
}

// MARK: - UIViewControllerTransitioningDelegate
extension HomeViewController: UIViewControllerTransitioningDelegate {
    func animationController(forPresented presented: UIViewController, presenting: UIViewController, source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        return PresentMenuAnimator()
    }

    func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        return DismissMenuAnimator()
    }

    func interactionControllerForDismissal(using animator: UIViewControllerAnimatedTransitioning) -> UIViewControllerInteractiveTransitioning? {
        return interactor.hasStarted ? interactor : nil
    }

    func interactionControllerForPresentation(using animator: UIViewControllerAnimatedTransitioning) -> UIViewControllerInteractiveTransitioning? {
        return interactor.hasStarted ? interactor : nil
    }
}

// MARK: - UITableViewDataSource
extension HomeViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return campaignStores.count
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(
            withIdentifier: CollectionViewTableViewCell.id,
            for: indexPath) as! CollectionViewTableViewCell

        return cell
    }

    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        guard campaignStores.count > 0 else { return nil }
        return campaignStores[section].sectionHeader()
    }
}

// MARK: - UITableViewDelegate
extension HomeViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        guard let cell = cell as? CollectionViewTableViewCell else { return }
        cell.setCollectionViewDataSource(campaignStores[indexPath.section], forSection: indexPath.section)
        cell.collectionViewOffset = storedOffsets[indexPath.row] ?? 0
        cell.delegate = self
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 120
    }
}

// MARK: - CollectionViewTableViewCellDelegate
extension HomeViewController: CollectionViewTableViewCellDelegate {
    func viewCampaignDetails(_ campaign: Campaign) {
        let destination = storyboard?.instantiateViewController(
            withIdentifier: CampaignDetailViewController.id) as! CampaignDetailViewController
        destination.campaign = campaign
        navigationController?.show(destination, sender: self)
    }
}

extension HomeViewController: MenuDelegate {}
