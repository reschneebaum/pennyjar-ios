//
//  UpdatesViewController.swift
//  PennyJar
//
//  Created by Rachel Schneebaum on 1/24/18.
//  Copyright © 2018 Rachel Schneebaum. All rights reserved.
//

import UIKit

class UpdatesViewController: UIViewController {
    var updates: [Update] = []

    @IBOutlet private weak var tableView: UITableView!

    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
        refresh()
    }

    func refresh() {
        guard let campaign = CampaignService.shared.currentCampaign else { return }
        CampaignService.shared.getUpdates(for: campaign) { [weak self] result in
            guard let strongSelf = self else { return }
            guard case .success(let updates) = result else { return }
            strongSelf.updates = updates
            strongSelf.tableView.reloadData()
        }
    }

    func deleteUpdate(at indexPath: IndexPath) {
        CampaignService.shared.deleteUpdate(updates[indexPath.row], from: CampaignService.shared.currentCampaign!) {
            [weak self] result in
            guard let strongSelf = self else { return }
            switch result {
            case .success(let newUpdates):
                strongSelf.updates = newUpdates
                strongSelf.tableView.deleteRows(at: [indexPath], with: .fade)

            case .failure(let error):
                strongSelf.errorAlert(error.localizedDescription)
            }
        }
    }

    func errorAlert(_ message: String) {
        let alert = UIAlertController(title: LocalizedStrings.unableToDelete, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: LocalizedStrings.ok, style: .default, handler: nil))
        present(alert, animated: true, completion: nil)
    }

    @objc func back() {
        dismiss(animated: true, completion: nil)
    }
}

// MARK: - UITableViewDataSource
extension UpdatesViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return updates.count == 0 ? 1 : updates.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: UpdateTableViewCell.id,
                                                 for: indexPath) as! UpdateTableViewCell
        if updates.count > 0 {
            cell.setup(update: updates[indexPath.row])
        } else {
            cell.setupForEmpty()
        }
        return cell
    }
}

// MARK: - UITableViewDelegate
extension UpdatesViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }

    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 72
    }

    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        if updates.count == 0 {
            return false
        } else {
            return true
        }
    }

    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        guard updates.count > 0 else { return }

        let alert = UIAlertController(title: LocalizedStrings.shouldDeleteUpdate, message: nil, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: LocalizedStrings.ok, style: .default) { _ in
            self.deleteUpdate(at: indexPath)
        })
        alert.addAction(UIAlertAction(title: LocalizedStrings.cancel, style: .cancel, handler: nil))
        present(alert, animated: true, completion: nil)
    }
}

// MARK: - private
private extension UpdatesViewController {
    func setup() {
        title = LocalizedStrings.updates
        let backButton = UIBarButtonItem(title: LocalizedStrings.back,
                                         style: .plain,
                                         target: self,
                                         action: #selector(back))
        navigationItem.leftBarButtonItem = backButton
        setupTableView()
    }

    func setupTableView() {
        tableView.tableFooterView = UIView(frame: CGRect.zero)
        tableView.separatorColor = .white

        tableView.register(UINib(nibName: UpdateTableViewCell.id, bundle: nil),
                           forCellReuseIdentifier: UpdateTableViewCell.id)
    }
}
