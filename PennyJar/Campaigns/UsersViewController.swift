//
//  UsersViewController.swift
//  PennyJar
//
//  Created by Rachel Schneebaum on 1/24/18.
//  Copyright © 2018 Rachel Schneebaum. All rights reserved.
//

import UIKit

private let userCellID = "UserTableViewCell"

class UsersViewController: UIViewController {

    var users: [User] = []

    @IBOutlet private weak var tableView: UITableView!

    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
        refresh()
    }

    func refresh() {
        guard let campaign = CampaignService.shared.currentCampaign else { return }
        CampaignService.shared.getUsers(for: campaign) { [weak self] result in
            guard let strongSelf = self else { return }
            guard case .success(let users) = result else { return }
            strongSelf.users = users
            strongSelf.tableView.reloadData()

            if users.count > 0 {
                strongSelf.tableView.separatorStyle = .singleLine
            }
        }
    }

    @objc func back() {
        dismiss(animated: true, completion: nil)
    }
}

// MARK: - UITableViewDataSource
extension UsersViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return users.count == 0 ? 1 : users.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return setupCell(at: indexPath)
    }
}

// MARK: - UITableViewDelegate
extension UsersViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
}

// MARK: - private
private extension UsersViewController {
    func setup() {
        title = LocalizedStrings.users
        let backButton = UIBarButtonItem(title: LocalizedStrings.back,
                                         style: .plain,
                                         target: self,
                                         action: #selector(back))
        navigationItem.leftBarButtonItem = backButton

        tableView.tableFooterView = UIView(frame: CGRect.zero)
    }

    func setupCell(at indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: userCellID, for: indexPath)

        guard users.count > 0 else {
            setupPlaceholderCell(cell)
            return cell
        }

        cell.accessoryType = .disclosureIndicator
        cell.textLabel?.textColor = .black
        cell.textLabel?.font = UIFont.systemFont(ofSize: 17)
        cell.textLabel?.textAlignment = .left

        if let first = users[indexPath.row].firstName {
            cell.textLabel?.text = first
            if let last = users[indexPath.row].lastName {
                cell.textLabel?.text?.append(" \(last)")
            }
        }

        return cell
    }

    func setupPlaceholderCell(_ cell: UITableViewCell) {
        cell.textLabel?.text = LocalizedStrings.noUsers
        cell.textLabel?.textColor = .lightGray
        cell.textLabel?.font = UIFont.italicSystemFont(ofSize: 15)
        cell.textLabel?.textAlignment = .center
        cell.accessoryType = .none
        cell.selectionStyle = .none
        tableView.separatorStyle = .none
    }
}
