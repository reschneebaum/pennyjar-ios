//
//  CampaignCollectionViewCell.swift
//  PennyJar
//
//  Created by Rachel Schneebaum on 1/24/18.
//  Copyright © 2018 Rachel Schneebaum. All rights reserved.
//

import UIKit

class CampaignCollectionViewCell: UICollectionViewCell {

    @IBOutlet private weak var imageView: UIImageView!

    func setImage(_ image: UIImage?, orURLString string: String?) {
        if let image = image {
            imageView.image = image
        } else if let urlString = string,
            let url = URL(string: urlString) {

            do {
                let data = try Data(contentsOf: url)
                if let image = UIImage(data: data) {
                    imageView.image = image
                }

            } catch {
                print("error setting image = \(error)")
            }
        }
    }
}
