//
//  CollectionViewTableViewCell.swift
//  PennyJar
//
//  Created by Rachel Schneebaum on 1/23/18.
//  Copyright © 2018 Rachel Schneebaum. All rights reserved.
//

import UIKit

protocol CollectionViewTableViewCellDelegate: class {
    func viewCampaignDetails(_ campaign: Campaign)
}

class CollectionViewTableViewCell: UITableViewCell {

    var delegate: CollectionViewTableViewCellDelegate?
    var collectionViewOffset: CGFloat {
        get {
            return collectionView.contentOffset.x
        }
        set {
            collectionView.contentOffset.x = newValue
        }
    }

    @IBOutlet private weak var collectionView: UICollectionView! {
        didSet {
            collectionView.register(UINib(nibName: CampaignCollectionViewCell.id, bundle: nil),
                                    forCellWithReuseIdentifier: CampaignCollectionViewCell.id)
            let recognizer = UITapGestureRecognizer(target: self, action: #selector(collectionViewTapped(_:)))
            collectionView.addGestureRecognizer(recognizer)
        }
    }
    
    @IBOutlet internal weak var noModelLabel: UILabel! {
        didSet {
            noModelLabel.text = LocalizedStrings.noCampaigns
            noModelLabel.alpha = 0
            noModelLabel.isHidden = true
        }
    }
    
    func setCollectionViewDataSource(_ campaignStore: CampaignStore, forSection section: Int) {
        collectionView.dataSource = campaignStore as! UICollectionViewDataSource
        collectionView.delegate = campaignStore as! UICollectionViewDelegate
        collectionView.tag = section
        collectionView.reloadData()

        if campaignStore.campaigns.count == 0 {
            setupForEmpty()
        } else {
            resetForModel()
        }
    }

    @objc func collectionViewTapped(_ sender: UITapGestureRecognizer) {
        let view = sender.view
        let location = sender.location(in: view)
        if let indexPath = collectionView.indexPathForItem(at: location) {
            if let campaignStore = collectionView.dataSource as? CampaignStore {
                let campaign = campaignStore.campaigns[indexPath.row]
                delegate?.viewCampaignDetails(campaign)
            }
        }
    }
}

extension CollectionViewTableViewCell: Emptiable {}
