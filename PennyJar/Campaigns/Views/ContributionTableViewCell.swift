//
//  ContributionTableViewCell.swift
//  PennyJar
//
//  Created by Rachel Schneebaum on 3/2/18.
//  Copyright © 2018 Rachel Schneebaum. All rights reserved.
//

import UIKit

class ContributionTableViewCell: UITableViewCell {

    @IBOutlet private weak var nameLabel: UILabel!
    @IBOutlet private weak var amountLabel: UILabel!
    @IBOutlet internal weak var noModelLabel: UILabel! {
        didSet {
            noModelLabel.text = LocalizedStrings.noContributions
            noModelLabel.alpha = 0
            noModelLabel.isHidden = true
        }
    }
    
    func setup(contribution: Contribution) {
        resetForModel()

        if let first = contribution.user?.firstName {
            nameLabel.text = first
            if let last = contribution.user?.lastName {
                nameLabel.text?.append(" \(last)")
            }
        }

        if let amount = contribution.amount {
            amountLabel.text = "$\(amount)"
        }
    }
}

extension ContributionTableViewCell: Emptiable {}
