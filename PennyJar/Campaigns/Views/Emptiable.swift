//
//  Emptiable.swift
//  PennyJar
//
//  Created by Rachel Schneebaum on 3/2/18.
//  Copyright © 2018 Rachel Schneebaum. All rights reserved.
//

import UIKit

protocol Emptiable {
    var noModelLabel: UILabel! { get set }
    func setupForEmpty()
    func resetForModel()
}

extension Emptiable where Self: UITableViewCell {
    func setupForEmpty() {
        contentView.subviews.forEach { $0.isHidden = true }
        noModelLabel.textColor = .lightGray
        noModelLabel.font = UIFont.italicSystemFont(ofSize: 15)
        noModelLabel.alpha = 0
        noModelLabel.isHidden = false
        UIView.animate(withDuration: 0.3) {
            self.noModelLabel.alpha = 1
        }
    }

    func resetForModel() {
        contentView.subviews.forEach { $0.isHidden = false }
        noModelLabel.isHidden = true
    }
}
