//
//  UpdateTableViewCell.swift
//  PennyJar
//
//  Created by Rachel Schneebaum on 3/1/18.
//  Copyright © 2018 Rachel Schneebaum. All rights reserved.
//

import UIKit

class UpdateTableViewCell: UITableViewCell {

    @IBOutlet private weak var roundedView: UIView! {
        didSet {
            roundedView.layer.cornerRadius = 12
        }
    }
    @IBOutlet private weak var titleLabel: UILabel!
    @IBOutlet private weak var messageImageView: UIImageView!
    @IBOutlet private weak var timestampLabel: UILabel!
    @IBOutlet private weak var descriptionLabel: UILabel!
    @IBOutlet internal weak var noModelLabel: UILabel! {
        didSet {
            noModelLabel.text = LocalizedStrings.noUpdates
            noModelLabel.alpha = 0
            noModelLabel.isHidden = true
        }
    }

    func setup(update: Update) {
        resetForModel()
        titleLabel.text = update.type?.title ?? ""
        descriptionLabel.text = update.description ?? update.user?.firstName ?? ""
        timestampLabel.text = update.timestamp
        messageImageView.isHidden = false
    }
}

extension UpdateTableViewCell: Emptiable {}
