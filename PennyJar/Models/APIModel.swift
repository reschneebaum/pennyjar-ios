//
//  APIModel.swift
//  PennyJar
//
//  Created by Rachel Schneebaum on 3/1/18.
//  Copyright © 2018 Rachel Schneebaum. All rights reserved.
//

import Foundation
import SwiftyJSON

protocol APIModel {
    init?(json: JSON)
}
