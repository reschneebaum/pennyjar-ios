//
//  Campaign.swift
//  PennyJar
//
//  Created by Rachel Schneebaum on 1/18/18.
//  Copyright © 2018 Rachel Schneebaum. All rights reserved.
//

import Foundation
import SwiftyJSON

struct Campaign {
    var slug: String?
    var title: String?
    var description: String?
    var goalString: String
    var endsAt: EndsAt?
    var actionRequired: Bool?
    var creator: User?
    var imageURLString: String?
    var urlString: String?
    var endsAtDescription: String?
    var actionURLString: String?
    var status: Status?
    var currentAmountString: String

    var currentAmount: Double {
        return Double(currentAmountString) ?? 0.0
    }

    var goal: Double {
        return Double(goalString) ?? 0.0
    }
}

extension Campaign: APIModel {
    init?(json: JSON) {
        title = json["title"].string
        slug = json["slug"].string
        if let htmlDescription = json["description"].string {
            description = htmlDescription.htmlToString
        }
        goalString = json["goal"].string ?? "0.00"
        endsAt = EndsAt(json: json["ends_at"])
        creator = User(json: json["creator"])
        actionRequired = json["action_required"].bool
        if let path = json["image"].string {
            imageURLString = Identifier.baseURLString + path
        }
        urlString = json["url"].string
        endsAtDescription = json["ends_at_str"].string
        actionURLString = json["action_url"].string

        if let statusString = json["status"].string {
            status = Status(rawValue: statusString)
        }
        currentAmountString = json["amount"].string ?? "0.00"
    }
}

struct EndsAt {
    var dateString: String?
    var date: Date?
    var timezone: String?

    init?(json: JSON) {
        dateString = json["date"].string
        timezone = json["timezone"].string
    }
}

enum Status: String {
    case active, pending, closed
}
