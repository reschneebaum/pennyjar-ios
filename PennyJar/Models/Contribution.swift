//
//  Contribution.swift
//  PennyJar
//
//  Created by Rachel Schneebaum on 3/2/18.
//  Copyright © 2018 Rachel Schneebaum. All rights reserved.
//

import Foundation
import SwiftyJSON

struct Contribution {
    var id: String?
    var user: User?
    var amount: String?
    var createdAt: EndsAt?
    var date: String?
}

extension Contribution: APIModel {
    init?(json: JSON) {
        id = json["id"].string
        user = User(json: json["user"])
        amount = json["amount"].string
        createdAt = EndsAt(json: json["created_at"])
        date = createdAt?.dateString
    }
}
