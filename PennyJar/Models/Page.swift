//
//  Page.swift
//  PennyJar
//
//  Created by Rachel Schneebaum on 2/26/18.
//  Copyright © 2018 Rachel Schneebaum. All rights reserved.
//

import Foundation

enum Page {
    case privacy, terms

    var title: String {
        switch self {
        case .privacy:
            return LocalizedStrings.privacy
        case .terms:
            return LocalizedStrings.terms
        }
    }
}
