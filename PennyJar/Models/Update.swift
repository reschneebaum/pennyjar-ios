//
//  Update.swift
//  PennyJar
//
//  Created by Rachel Schneebaum on 2/27/18.
//  Copyright © 2018 Rachel Schneebaum. All rights reserved.
//

import Foundation
import SwiftyJSON

struct Update {
    var id: String?
    var type: Type?
    var user: User?
    var description: String?
    var timestamp: String?
    var timestampDate: Date?

    enum `Type`: String {
        case userLeft = "user_left"
        case userJoined = "user_joined"
        case jarUpdate = "jar_update"

        var title: String {
            switch self {
            case .userLeft:   return "User Left"
            case .userJoined: return "User Joined"
            case .jarUpdate:  return "Jar Update"
            }
        }
    }
}

extension Update: APIModel {
    init?(json: JSON) {
        id = json["slug"].string
        user = User(json: json["user"])
        timestamp = json["date"].string
        description = json["description"].string
        if let typeString = json["type"].string {
            type = Type(rawValue: typeString)
        }
    }
}
