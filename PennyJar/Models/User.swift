//
//  User.swift
//  PennyJar
//
//  Created by Rachel Schneebaum on 8/26/17.
//  Copyright © 2017 Rachel Schneebaum. All rights reserved.
//

import UIKit
import SwiftyJSON

struct User {
    var slug: String?
    var firstName: String?
    var lastName: String?
    var avatarUrlString: String?
    var email: String?
    var token: String?

    var avatar: UIImage? {
        guard let urlString = avatarUrlString,
            let url = URL(string: urlString) else { return nil }

        print("avatar url: \(urlString)")

        do {
            let data = try Data(contentsOf: url)
            return UIImage(data: data)

        } catch {
            print("error = \(error.localizedDescription)")
            return nil
        }
    }
}

extension User: APIModel {
    init?(json: JSON) {
        slug = json["slug"].string
        firstName = json["first_name"].string
        lastName = json["last_name"].string
        avatarUrlString = json["image"].string
        email = json["token"]["email"].string
        token = json["token"]["token"].string
    }
}
