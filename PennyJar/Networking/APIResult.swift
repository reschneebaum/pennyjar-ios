//
//  APIResult.swift
//  PennyJar
//
//  Created by Rachel Schneebaum on 8/26/17.
//  Copyright © 2017 Rachel Schneebaum. All rights reserved.
//

import Foundation

enum APIError: Error {
    case returned(String?)
    case noAuthToken
    case unableToRequest
}

enum APIResult<T> {
    case success(T)
    case failure(APIError)
}

enum OAuthError: Error {
    case noCallback
    case noOAuthCode
    case noBearerToken
    case other(String?)
}

enum OAuthResult<T> {
    case success(T)
    case failure(OAuthError)
}
