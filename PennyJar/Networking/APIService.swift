//
//  APIService.swift
//  PennyJar
//
//  Created by Rachel Schneebaum on 3/1/18.
//  Copyright © 2018 Rachel Schneebaum. All rights reserved.
//

import Foundation
import SwiftyJSON
import Alamofire

protocol APIService {
    func parseJSONArray<T: APIModel>(_ json: JSON) -> [T]
    func parseErrorResponse<T: Any>(_ response: DataResponse<Any>) -> APIResult<T>
}

extension APIService {
    func parseJSONArray<T: APIModel>(_ json: JSON) -> [T] {
        guard let total = json["meta"]["pagination"]["total"].int, total > 0 else {
            return []
        }

        var models: [T] = []
        for i in 0..<total {
            if let newModel = T(json: json["\(i)"]) {
                models.append(newModel)
            }
        }
        return models
    }

    func parseErrorResponse<T: Any>(_ response: DataResponse<Any>) -> APIResult<T> {
        guard let data = response.data else {
            return .failure(.returned(nil))
        }

        let json = JSON(data: data)
        print(json)

        return .failure(.returned(json["error"].string))
    }
}
