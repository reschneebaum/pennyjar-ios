//
//  AuthService.swift
//  PennyJar
//
//  Created by Rachel Schneebaum on 8/26/17.
//  Copyright © 2017 Rachel Schneebaum. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

class AuthService: APIService {

    static var shared = AuthService()
    private init() {
        manager = Alamofire.SessionManager.default
    }

    var currentUser: User?

    private var manager: Alamofire.SessionManager


    func login(completion: @escaping (APIResult<String>) -> Void) {
        manager.request(PJRouter.getLoginURL())
            .validate()
            .responseJSON { response in
                switch response.result {
                case .success(let value):
                    let json = JSON(value)
                    print("login json: \(json)")

                    if let url = json["url"].string {
                        completion(.success(url))
                    }

                case .failure(_):
                    print("login error json:")
                    completion(self.parseErrorResponse(response))
                }
        }
    }

    func sendAuthToken(_ token: String, completion: @escaping (APIResult<User>) -> Void) {
        manager.request(PJRouter.postAuthToken(token))
            .validate()
            .responseJSON { response in
                switch response.result {
                case .success(let value):
                    let json = JSON(value)
                    print("callback response json: \(json)")
                    
                    if let user = User(json: json) {
                        if let token = user.token {
                            _ = Keychain.set(token, forKey: Keys.bearerToken)
                        }
                        
                        self.currentUser = user
                        completion(.success(user))
                    }

                case .failure(_):
                    if let data = response.data {
                        let json = JSON(data: data)
                        print("callback error json: \(json)")
                        completion(.failure(.returned(json.string)))
                    }
                }
        }
    }

    func refresh(completion: @escaping (APIResult<User>) -> Void) {
        manager.request(PJRouter.refreshToken())
            .validate()
            .responseJSON { response in
                switch response.result {
                case .success(let value):
                    let json = JSON(value)
                    print("refresh json: \(json)")

                    if let user = User(json: json) {
                        if let token = user.token {
                            _ = Keychain.set(token, forKey: Keys.bearerToken)
                        }

                        self.currentUser = user
                        completion(.success(user))
                    }

                case .failure(_):
                    print("refresh error json:")
                    completion(self.parseErrorResponse(response))
                }
        }
    }

    func logout(completion: @escaping (APIResult<Bool>) -> Void) {
        manager.request(PJRouter.logout())
            .validate()
            .responseJSON { response in
                switch response.result {
                case .success(_):
                    completion(.success(true))

                case .failure(_):
                    print("logout error json:")
                    completion(self.parseErrorResponse(response))
                }
        }
    }

    func getStaticPage(_ page: Page, completion: @escaping (APIResult<String>) -> Void) {
        manager.request(PJRouter.getPage(page))
            .validate()
            .responseString { response in
                switch response.result {
                case .success(let value):
                    print(value)
                    completion(.success(value))
                case .failure(let error):
                    print(error)
                    completion(.failure(.returned(error.localizedDescription)))
                }
            }
            .responseJSON { response in
                switch response.result {
                case .success(let value):
                    let json = JSON(value)
                    print("page json: \(json)")

                case .failure(_):
                    print("page error json:")
                    completion(self.parseErrorResponse(response))
                }
        }
    }
}
