//
//  CampaignService.swift
//  PennyJar
//
//  Created by Rachel Schneebaum on 1/23/18.
//  Copyright © 2018 Rachel Schneebaum. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

class CampaignService: APIService {
    var currentCampaign: Campaign?

    static var shared = CampaignService()
    private init() {
        manager = Alamofire.SessionManager.default
    }

    private var manager: Alamofire.SessionManager

    enum ReturnedType {
        case user, other, closed, all
    }

    func getCampaigns(type: ReturnedType, completion: @escaping (APIResult<[Campaign]>) -> Void) {
        var request: URLRequestConvertible
        switch type {
        case .all:      request = PJRouter.getCampaigns()
        case .user:     request = PJRouter.getOpenCampaigns()
        case .other:    request = PJRouter.getOtherCampaigns()
        case .closed:   request = PJRouter.getClosedCampaigns()
        }

        manager.request(request)
            .validate()
            .responseJSON { response in
                switch response.result {
                case .success(let value):
                    let json = JSON(value)
                    print("campaigns json: \(json)")

                    let campaigns: [Campaign] = self.parseJSONArray(json)
                    completion(.success(campaigns))

                case .failure(_):
                    print("campaigns error json:")
                    completion(self.parseErrorResponse(response))
                }
        }
    }

    func createNewCampaign(_ info: [String: String], with image: UIImage?, completion: @escaping (APIResult<Bool>) -> Void) {
        manager.upload(multipartFormData: { formData in
            for (key, value) in info {
                formData.append(value.data(using: .utf8)!, withName: key)
            }
            if let image = image, let imageData = UIImageJPEGRepresentation(image, 0.8) {
                formData.append(imageData, withName: "image", fileName: "image", mimeType: "image/jpeg")
            }
        }, with: PJRouter.createCampaign()) { result in
            switch result {
            case .success(request: let upload, streamingFromDisk: _, streamFileURL: _):
                upload.response { response in
                    print(response)

                    guard let code = response.response?.statusCode,
                        code < 300 else {
                            return completion(.failure(APIError.returned(response.error?.localizedDescription)))
                    }

                    completion(.success(true))
                }

            case .failure(let encodingError):
                print("error encoding data: \(encodingError)")
                completion(.failure(APIError.returned(encodingError.localizedDescription)))
            }
        }
    }

    func addContribution(to campaign: Campaign, amount: String, completion: @escaping (APIResult<Campaign>) -> Void) {
        guard let slug = campaign.slug else { return completion(.failure(APIError.unableToRequest)) }

        manager.request(PJRouter.contribute(slug, amount))
            .validate()
            .responseJSON { response in
                switch response.result {
                case .success(let value):
                    let json = JSON(value)
                    print("contribute json: \(json)")
                    if let campaign = Campaign(json: json) {
                        completion(.success(campaign))
                    }

                case .failure(_):
                    print("contribute error json:")
                    completion(self.parseErrorResponse(response))
                }
        }
    }

    func getUpdates(for campaign: Campaign, completion: @escaping (APIResult<[Update]>) -> Void) {
        guard let slug = campaign.slug else { return completion(.failure(APIError.unableToRequest)) }
        manager.request(PJRouter.getUpdates(slug))
            .validate()
            .responseJSON { response in
                switch response.result {
                case .success(let value):
                    let json = JSON(value)
                    print("updates json: \(json)")

                    let updates: [Update] = self.parseJSONArray(json)
                    completion(.success(updates))

                case .failure(_):
                    print("updates error json:")
                    completion(self.parseErrorResponse(response))
                }
        }
    }

    func deleteUpdate(_ update: Update, from campaign: Campaign, completion: @escaping (APIResult<[Update]>) -> Void) {
        guard let updateID = update.id,
            let slug = campaign.slug else {
                return completion(.failure(.unableToRequest))
        }
        manager.request(PJRouter.deleteUpdate(slug, updateID))
            .validate()
            .responseJSON { response in
                switch response.result {
                case .success(let value):
                    let json = JSON(value)
                    print("delete update json: \(json)")

                    let updates: [Update] = self.parseJSONArray(json)
                    completion(.success(updates))

                case .failure(_):
                    print("delete update error json:")
                    completion(self.parseErrorResponse(response))
                }
        }
    }

    func getContributions(for campaign: Campaign, completion: @escaping (APIResult<[Contribution]>) -> Void) {
        guard let slug = campaign.slug else { return completion(.failure(APIError.unableToRequest)) }
        manager.request(PJRouter.getContributions(slug))
            .validate()
            .responseJSON { response in
                switch response.result {
                case .success(let value):
                    let json = JSON(value)
                    print("contributions json: \(json)")

                    let contributions: [Contribution] = self.parseJSONArray(json)
                    completion(.success(contributions))

                case .failure(_):
                    print("contributions error json:")
                    completion(self.parseErrorResponse(response))
                }
        }
    }

    func getUsers(for campaign: Campaign, completion: @escaping (APIResult<[User]>) -> Void) {
        guard let slug = campaign.slug else { return completion(.failure(APIError.unableToRequest)) }
        manager.request(PJRouter.getUsers(slug))
            .validate()
            .responseJSON { response in
                switch response.result {
                case .success(let value):
                    let json = JSON(value)
                    print("users json: \(json)")

                    let users: [User] = self.parseJSONArray(json)
                    completion(.success(users))

                case .failure(_):
                    print("users error json:")
                    completion(self.parseErrorResponse(response))
                }
        }
    }
}
