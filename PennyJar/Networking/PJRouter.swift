//
//  PJRouter.swift
//  PennyJar
//
//  Created by Rachel Schneebaum on 8/25/17.
//  Copyright © 2017 Rachel Schneebaum. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

enum PJRouter: URLRequestConvertible {

    static let baseUrlString = "http://dev.pennyjar.io/api/"

    // auth
    case getLoginURL()
    case postAuthToken(String)
    case getLoadingPlaceholder()
    case refreshToken()
    case logout()

    // profiles
    case getCurrentUserProfile()
    case updateProfile()
    case getOtherProfile(String)
    case searchUsers(String)

    // campaigns
    case getCampaigns()
    case getOpenCampaigns()
    case getOtherCampaigns()
    case getClosedCampaigns()
    case createCampaign()
    case getUpdates(String)
    case deleteUpdate(String, String)
    case createNewUpdate(String)
    case getContributions(String)
    case contribute(String, String)
    case getUsers(String)
    case inviteUser(String, String)

    // pages
    case getPage(Page)

    func asURLRequest() throws -> URLRequest {

        var method: HTTPMethod {
            switch self {
            case .getLoginURL, .getLoadingPlaceholder, .getCurrentUserProfile, .getOtherProfile, .searchUsers,
                 .getCampaigns, .getOpenCampaigns, .getOtherCampaigns, .getClosedCampaigns,
                 .getUpdates, .getContributions, .getUsers,
                 .getPage:
                return .get
            case .logout, .postAuthToken, .refreshToken,
                 .updateProfile,
                 .createCampaign, .contribute, .createNewUpdate, .inviteUser    :
                return .post
            case .deleteUpdate:
                return .delete
            }
        }

        let params: ([String: Any]?) = {
            switch self {
            case .postAuthToken(let code):
                return (["code": code])
            case .contribute(_, let amount):
                return (["amount": amount])

            default: return nil
            }
        }()

        let url: URL = {
            let relativePath: String?
            switch self {
            case .getLoginURL:
                relativePath = "auth/login"
            case .postAuthToken:
                relativePath = "auth/login"
            case .getLoadingPlaceholder:
                relativePath = "auth/loading"
            case .logout:
                relativePath = "auth/logout"
            case .refreshToken:
                relativePath = "auth/refresh"

            case .getCurrentUserProfile:
                relativePath = "user"
            case .updateProfile:
                relativePath = "user"
            case .getOtherProfile(let slug):
                relativePath = "user/\(slug)"
            case .searchUsers(let searchTerm):
                relativePath = "search/\(searchTerm)"

            case .getCampaigns:
                relativePath = "campaigns"
            case .getOpenCampaigns:
                relativePath = "campaigns/user"
            case .getOtherCampaigns:
                relativePath = "campaigns/other"
            case .getClosedCampaigns:
                relativePath = "campaigns/completed"
            case .createCampaign:
                relativePath = "campaigns"
            case .getUpdates(let slug):
                relativePath = "campaigns/\(slug)/updates"
            case .createNewUpdate(let slug):
                relativePath = "campaigns/\(slug)/updates"
            case .deleteUpdate(let slug, let updateID):
                relativePath = "campaigns/\(slug)/updates/\(updateID)"
            case .getContributions(let slug):
                relativePath = "campaigns/\(slug)/contributions"
            case .contribute(let slug, _):
                relativePath = "campaigns/\(slug)/contributions"
            case .getUsers(let slug):
                relativePath = "campaigns/\(slug)/users"
            case .inviteUser(let slug, _):
                relativePath = "campaigns/\(slug)/users/invite"

            case .getPage(let page):
                switch page {
                case .privacy:
                    relativePath = "pages/privacy"
                case .terms:
                    relativePath = "pages/terms"
                }
            }

            var urlPath = URL(string: PJRouter.baseUrlString)!
            if let path = relativePath {
                urlPath = urlPath.appendingPathComponent(path)
            }

            return urlPath
        }()

        var urlRequest = URLRequest(url: url)
        urlRequest.httpMethod = method.rawValue

        let encoding: Alamofire.ParameterEncoding = JSONEncoding.default
        switch self {
        case .getPage:
            urlRequest.setValue("text/html", forHTTPHeaderField: "Accept")
        default:
            urlRequest.setValue("application/json", forHTTPHeaderField: "Accept")
        }

        switch self {
        case .getLoginURL:
            break
        default:
            if let token = Keychain.value(forKey: "token") {
                urlRequest.setValue("Bearer \(token)", forHTTPHeaderField: "Authorization")
            }
        }

        return try encoding.encode(urlRequest, with: params)
    }
}
