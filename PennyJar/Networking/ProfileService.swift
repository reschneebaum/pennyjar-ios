//
//  ProfileService.swift
//  PennyJar
//
//  Created by Rachel Schneebaum on 9/10/17.
//  Copyright © 2017 Rachel Schneebaum. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

class ProfileService: APIService {

    static let shared = ProfileService()
    private init() {
        manager = Alamofire.SessionManager.default
    }

    private var manager: Alamofire.SessionManager

    func getProfile(completion: @escaping (APIResult<User>) -> Void) {
        manager.request(PJRouter.getCurrentUserProfile())
            .validate()
            .responseJSON { response in
                switch response.result {
                case .success(let value):
                    let json = JSON(value)
                    print("get profile json: \(json)")

                    if let user = User(json: json) {
                        completion(.success(user))
                    }

                case .failure(_):
                    print("profile error json:")
                    completion(self.parseErrorResponse(response))
                }
        }
    }

    func getOtherProfile(for id: String, completion: @escaping (APIResult<User>) -> Void) {
        manager.request(PJRouter.getOtherProfile(id))
            .validate()
            .responseJSON { response in
                switch response.result {
                case .success(let value):
                    let json = JSON(value)
                    print("get profile json: \(json)")

                    if let user = User(json: json) {
                        completion(.success(user))
                    }

                case .failure(_):
                    print("profile error json:")
                    completion(self.parseErrorResponse(response))
                }
        }
    }

    func updateProfile(_ update: User, avatar: UIImage?, completion: @escaping (APIResult<Bool>) -> Void) {
        manager.upload(multipartFormData: { formData in
            if let first = update.firstName {
                formData.append(first.data(using: .utf8)!, withName: "first_name")
            }
            if let last = update.lastName {
                formData.append(last.data(using: .utf8)!, withName: "last_name")
            }
            if let email = update.email {
                formData.append(email.data(using: .utf8)!, withName: "email")
            }
            if let image = avatar, let imageData = UIImageJPEGRepresentation(image, 0.8) {
                formData.append(imageData, withName: "avatar", fileName: "avatar", mimeType: "image/jpeg")
            }
        }, with: PJRouter.updateProfile()) { result in
            switch result {
            case .success(request: let upload, streamingFromDisk: _, streamFileURL: _):
                upload.response { response in
                    print(response)

                    guard let code = response.response?.statusCode,
                        code < 300 else {
                            return completion(.failure(APIError.returned(response.error?.localizedDescription)))
                    }

                    completion(.success(true))
                }

            case .failure(let encodingError):
                print("error encoding data: \(encodingError)")
                completion(.failure(APIError.returned(encodingError.localizedDescription)))
            }
        }
    }

    func searchUsers(for searchTerm: String, completion: @escaping (APIResult<[User]>) -> Void) {
        manager.request(PJRouter.searchUsers(searchTerm))
            .validate()
            .responseJSON { response in
                switch response.result {
                case .success(let value):
                    let json = JSON(value)
                    print("search users json: \(json)")

                    let users: [User] = self.parseJSONArray(json)
                    completion(.success(users))

                case .failure(_):
                    print("search error json:")
                    completion(self.parseErrorResponse(response))
                }
        }
    }
}
