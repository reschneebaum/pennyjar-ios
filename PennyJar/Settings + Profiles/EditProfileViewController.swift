//
//  EditProfileViewController.swift
//  PennyJar
//
//  Created by Rachel Schneebaum on 1/23/18.
//  Copyright © 2018 Rachel Schneebaum. All rights reserved.
//

import UIKit

class EditProfileViewController: UIViewController {

    @IBOutlet private weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
    }
}

// MARK: - UITableViewDataSource
extension EditProfileViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:  return 1
        case 1:  return 3
        default: return 0
        }
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let user = AuthService.shared.currentUser else { print("no user logged in"); return UITableViewCell() }

        switch indexPath.section {
        case 0:
            let cell = tableView.dequeueReusableCell(
                withIdentifier: ProfileHeaderTableViewCell.id,
                for: indexPath) as! ProfileHeaderTableViewCell
            cell.configure(user: user)

            return cell

        default:
            let cell = tableView.dequeueReusableCell(
                withIdentifier: ProfileTextFieldTableViewCell.id,
                for: indexPath) as! ProfileTextFieldTableViewCell

            switch indexPath.row {
            case 0:
                cell.configure(for: user, field: .firstName, delegate: self)
            case 1:
                cell.configure(for: user, field: .lastName, delegate: self)
            case 2:
                cell.configure(for: user, field: .email, delegate: self)
            default: break
            }

            return cell
        }
    }

    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        switch section {
        case 1:  return "Basic Info"
        default: return nil
        }
    }
}

// MARK: - UITableViewDelegate
extension EditProfileViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.section {
        case 0:  return 200
        default: return 60
        }
    }

    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        switch section {
        case 0:  return 0.1
        default: return 20
        }
    }
}

// MARK: - UITextFieldDelegate
extension EditProfileViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.endEditing(true)
        return false
    }

    func textFieldDidEndEditing(_ textField: UITextField) {
        textField.resignFirstResponder()
    }
}

// MARK: - private
private extension EditProfileViewController {
    func setup() {
        title = "Edit Profile"

        setupTableView()
    }

    func setupTableView() {
        tableView.register(UINib(nibName: ProfileTextFieldTableViewCell.id, bundle: nil),
                           forCellReuseIdentifier: ProfileTextFieldTableViewCell.id)
        tableView.register(UINib(nibName: ProfileHeaderTableViewCell.id, bundle: nil),
                           forCellReuseIdentifier: ProfileHeaderTableViewCell.id)
        tableView.tableFooterView = UIView(frame: CGRect.zero)
    }
}
