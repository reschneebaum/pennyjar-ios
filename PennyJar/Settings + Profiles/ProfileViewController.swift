//
//  ProfileViewController.swift
//  PennyJar
//
//  Created by Rachel Schneebaum on 3/7/18.
//  Copyright © 2018 Rachel Schneebaum. All rights reserved.
//

import UIKit

class ProfileViewController: UIViewController {

    var user: User!
    var jars: [Campaign] = []
    var contributions: [Contribution] = []
    
    @IBOutlet private weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
        refresh()
    }

    func refresh() {
        ProfileService.shared.getProfile {
            [weak self] result in
            guard let strongSelf = self,
                case .success(let user) = result else { return }
            strongSelf.user = user
            strongSelf.tableView.reloadSections([0], with: .automatic)

            CampaignService.shared.getCampaigns(type: .all) {
                [weak self] campaignResult in
                guard let strongSelf = self,
                    case .success(let campaigns) = campaignResult else { return }
                strongSelf.jars = campaigns
                strongSelf.tableView.reloadSections([1], with: .automatic)

                campaigns.forEach {
                    CampaignService.shared.getContributions(for: $0) {
                        [weak self] contributionResult in
                        guard let strongSelf = self,
                            case .success(let contributions) = contributionResult else { return }
                        let myContributions = contributions.filter {
                            $0.user?.slug == strongSelf.user.slug
                        }
                        strongSelf.contributions.append(contentsOf: myContributions)
                        strongSelf.tableView.reloadSections([2], with: .automatic)
                    }
                }
            }
        }
    }
}

// MARK: - UITableViewDataSource
extension ProfileViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:  return user != nil ? 1 : 0
        case 1:  return jars.count
        case 2:  return contributions.count
        default: return 0
        }
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell: UITableViewCell
        switch indexPath.section {
        case 0:
            cell = tableView.dequeueReusableCell(withIdentifier: ProfileHeaderTableViewCell.id,
                                                 for: indexPath) as! ProfileHeaderTableViewCell
            (cell as! ProfileHeaderTableViewCell).configure(user: user)

        default:
            cell = configureCell(for: indexPath)
        }

        return cell
    }

    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        switch section {
        case 1:  return LocalizedStrings.myCampaigns
        case 2:  return LocalizedStrings.myContributions
        default: return nil
        }
    }
}

// MARK: - UITableViewDelegate
extension ProfileViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)

        guard indexPath.section == 1 else { return }

        let destination = storyboard?.instantiateViewController(
            withIdentifier: CampaignDetailViewController.id) as! CampaignDetailViewController
        destination.campaign = jars[indexPath.row]
        navigationController?.show(destination, sender: self)
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.section {
        case 0:  return 200
        default: return 60
        }
    }

    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        switch section {
        case 0:  return 0.1
        default: return 20
        }
    }
}

// MARK: - private
private extension ProfileViewController {
    var cellID: String {
        return "ProfileInfoTableViewCell"
    }

    func setup() {
        title = LocalizedStrings.profile
        setupTableView()
    }

    func setupTableView() {
        tableView.register(UINib(nibName: ProfileHeaderTableViewCell.id, bundle: nil),
                           forCellReuseIdentifier: ProfileHeaderTableViewCell.id)
        tableView.tableFooterView = UIView(frame: CGRect.zero)
    }

    func configureCell(for indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellID, for: indexPath)

        switch indexPath.section {
        case 1:
            cell.textLabel?.text = jars[indexPath.row].title ?? ""
            cell.detailTextLabel?.text = "$\(jars[indexPath.row].currentAmountString) of $\(jars[indexPath.row].goal) credits"
        case 2:
            if let amount = contributions[indexPath.row].amount {
                cell.textLabel?.text = "$\(amount)"
            }
            cell.detailTextLabel?.text = contributions[indexPath.row].date
        default:
            break
        }

        return cell
    }
}
