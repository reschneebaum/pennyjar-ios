//
//  SettingsViewController.swift
//  PennyJar
//
//  Created by Rachel Schneebaum on 1/19/18.
//  Copyright © 2018 Rachel Schneebaum. All rights reserved.
//

import UIKit

private let rows = [
    LocalizedStrings.viewProfile,
    LocalizedStrings.editProfile,
    LocalizedStrings.changePassword,
    LocalizedStrings.privacy,
    LocalizedStrings.terms,
    LocalizedStrings.logout
]

class SettingsViewController: UIViewController {

    @IBOutlet private weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
    }
}

// MARK: - UITableViewDataSource
extension SettingsViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return rows.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return configureCell(at: indexPath)
    }
}

// MARK: - UITableViewDelegate
extension SettingsViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)

        switch indexPath.row {
        case 0:
            presentViewController(ProfileViewController.self, sender: self)
        case 1:
            presentViewController(EditProfileViewController.self, sender: self)
        case 2:
            presentViewController(ChangePasswordViewController.self, sender: self)
        case 3:
            presentStaticViewController(.privacy, sender: self)
        case 4:
            presentStaticViewController(.terms, sender: self)
        case 5:
            logout()
        default:
            return
        }
    }
}

// MARK: - private
private extension SettingsViewController {
    var cellID: String {
        return "SettingsTableViewCell"
    }

    func setup() {
        title = LocalizedStrings.settings
        tableView.tableFooterView = UIView(frame: CGRect.zero)
    }

    func configureCell(at indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellID,
                                                 for: indexPath)
        cell.textLabel?.text = rows[indexPath.row]

        if indexPath.row == rows.count - 1 {
            cell.accessoryType = .none
        }

        return cell
    }
}

extension SettingsViewController: Presenter {}
extension SettingsViewController: Logoutable {}
