//
//  StaticViewController.swift
//  PennyJar
//
//  Created by Rachel Schneebaum on 1/19/18.
//  Copyright © 2018 Rachel Schneebaum. All rights reserved.
//

import UIKit
import WebKit

class StaticViewController: UIViewController {

    var page: Page = .terms

    override func viewDidLoad() {
        super.viewDidLoad()
        let webview = WKWebView()
        loadHTML(to: webview)
        view = webview
        title = page.title
    }

    func loadHTML(to webview: WKWebView) {
        AuthService.shared.getStaticPage(page) { result in
            switch result {
            case .success(let value):
                webview.loadHTMLString(value, baseURL: nil)
            case .failure(let error):
                print(error.localizedDescription)
            }
        }
    }
}
