//
//  ProfileHeaderTableViewCell.swift
//  PennyJar
//
//  Created by Rachel Schneebaum on 3/10/18.
//  Copyright © 2018 Rachel Schneebaum. All rights reserved.
//

import UIKit

class ProfileHeaderTableViewCell: UITableViewCell {
    
    @IBOutlet private weak var avatarImageView: UIImageView!
    @IBOutlet private weak var nameLabel: UILabel!
    
    func configure(user: User) {
        avatarImageView.layer.cornerRadius = avatarImageView.frame.height / 2
        avatarImageView.image = user.avatar

        if let first = user.firstName {
            nameLabel.text = first
            if let last = user.lastName {
                nameLabel.text?.append(" \(last)")
            }
        }
    }
}
