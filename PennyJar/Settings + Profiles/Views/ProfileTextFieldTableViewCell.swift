//
//  ProfileTextFieldTableViewCell.swift
//  PennyJar
//
//  Created by Rachel Schneebaum on 3/10/18.
//  Copyright © 2018 Rachel Schneebaum. All rights reserved.
//

import UIKit

class ProfileTextFieldTableViewCell: UITableViewCell {

    @IBOutlet private weak var placeholderLabel: UILabel!
    @IBOutlet private weak var textField: UITextField!

    func configure<T: UITextFieldDelegate>(for user: User, field: Field, delegate: T) {
        textField.delegate = delegate
        placeholderLabel.text = field.placeholder

        switch field {
        case .firstName:
            if let firstName = user.firstName {
                textField.text = firstName
            }
            textField.returnKeyType = .next
            textField.enablesReturnKeyAutomatically = true
            textField.autocapitalizationType = .words

        case .lastName:
            if let lastName = user.lastName {
                textField.text = lastName
            }
            textField.returnKeyType = .next
            textField.enablesReturnKeyAutomatically = true
            textField.autocapitalizationType = .words

        case .email:
            if let email = user.email {
                textField.text = email
            }
            textField.returnKeyType = .done
            textField.enablesReturnKeyAutomatically = true
            textField.autocapitalizationType = .none
            textField.keyboardType = .emailAddress
        }
    }
}

enum Field {
    case firstName, lastName, email

    var placeholder: String {
        switch self {
        case .firstName:    return "First Name"
        case .lastName:     return "Last Name"
        case .email:        return "Email"
        }
    }
}
