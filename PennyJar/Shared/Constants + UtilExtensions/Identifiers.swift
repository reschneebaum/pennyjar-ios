//
//  Identifiers.swift
//  PennyJar
//
//  Created by Rachel Schneebaum on 8/26/17.
//  Copyright © 2017 Rachel Schneebaum. All rights reserved.
//

import UIKit

extension UIViewController {
    static var id: String {
        return "\(self)"
    }
}

extension UIView {
    static var id: String {
        return "\(self)"
    }
}

public enum Identifier {
    static let baseURLString = "http://dev.pennyjar.io"
}
