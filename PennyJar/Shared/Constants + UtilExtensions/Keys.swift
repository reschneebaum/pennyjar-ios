//
//  Keys.swift
//  PennyJar
//
//  Created by Rachel Schneebaum on 9/10/17.
//  Copyright © 2017 Rachel Schneebaum. All rights reserved.
//

import Foundation

enum Keys {
    static let clientID = "clientID"
    static let oAuthCode = "oAuthCode"
    static let bearerToken = "token"
    static let client_id = "client_id"
    static let code = "code"
}

enum IDs {
    enum Segue {
        static let menu = "MenuSegue"
    }

    enum TabBarController {
        static let campaignDetail = "CampaignTabBarController"
    }

    enum NavigationController {
        static let main = "mainNavVC"
        static let web = "WebViewNavigationController"
        static let users = "UsersNavigationController"
        static let contributions = "ContributionsNavigationController"
        static let updates = "UpdatesNavigationController"
    }

    enum TableViewCell {
        static let menu = "MenuTableViewCell"
        static let campaign = "CampaignTableViewCell"
        static let placeholder = "PlaceholderTableViewCell"
    }
}
