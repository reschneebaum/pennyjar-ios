//
//  PJLocalizedStrings.swift
//  PennyJar
//
//  Created by Rachel Schneebaum on 9/11/17.
//  Copyright © 2017 Rachel Schneebaum. All rights reserved.
//

import Foundation

enum LocalizedStrings {
    // auth
    static let title = NSLocalizedString("PennyJar", comment: "")
    static let welcome = NSLocalizedString("Welcome", comment: "")
    static let loginButton = NSLocalizedString("WePay Sign In", comment: "")
    static let loggingIn = NSLocalizedString("logging in...", comment: "")
    // menu
    static let campaigns = NSLocalizedString("Jars", comment: "")
    static let notifications = NSLocalizedString("Notifications", comment: "")
    static let settings = NSLocalizedString("Settings", comment: "")
    static let privacy = NSLocalizedString("Privacy Policy", comment: "")
    static let terms = NSLocalizedString("Terms of Service", comment: "")
    static let logout = NSLocalizedString("Logout", comment: "")
    static let profile = NSLocalizedString("Profile", comment: "")
    static let viewProfile = NSLocalizedString("View Profile", comment: "")
    static let editProfile = NSLocalizedString("Edit Profile", comment: "")
    static let changePassword = NSLocalizedString("Change Password", comment: "")
    // campaigns
    static let noCampaigns = NSLocalizedString("no available jars", comment: "")
    static let myCampaigns = NSLocalizedString("MY JARS", comment: "")
    static let otherCampaigns = NSLocalizedString("OTHER JARS", comment: "")
    static let closedCampaigns = NSLocalizedString("CLOSED JARS", comment: "")
    static let users = NSLocalizedString("Users", comment: "")
    static let contributions = NSLocalizedString("Contributions", comment: "")
    static let updates = NSLocalizedString("Updates", comment: "")
    static let jarDetails = NSLocalizedString("Jar Details", comment: "")
    static let info = NSLocalizedString("Info", comment: "")
    static let contribute = NSLocalizedString("Contribute", comment: "")
    static let contributeToJar = NSLocalizedString("Contribute to Jar", comment: "")
    static let amount = NSLocalizedString("Amount", comment: "")
    static let noUpdates = NSLocalizedString("no updates", comment: "")
    static let noUsers = NSLocalizedString("no users", comment: "")
    static let noContributions = NSLocalizedString("no contributions", comment: "")
    static let unableToDelete = NSLocalizedString("Unable to delete", comment: "")
    static let shouldDeleteUpdate = NSLocalizedString("Delete Update?", comment: "")
    // profile
    static let myContributions = NSLocalizedString("My Contributions", comment: "")
    // common
    static let back = NSLocalizedString("Back", comment: "")
    static let ok = NSLocalizedString("Ok", comment: "")
    static let cancel = NSLocalizedString("Cancel", comment: "")
}
