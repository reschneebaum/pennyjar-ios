//
//  PJUtilExtensions.swift
//  PennyJar
//
//  Created by Rachel Schneebaum on 1/18/18.
//  Copyright © 2018 Rachel Schneebaum. All rights reserved.
//

import Foundation

public extension URL {
    public func queryParameterValue(for key: String) -> String? {
        guard let url = URLComponents(string: absoluteString) else { return nil }
        return url.queryItems?.first(where: { $0.name == key })?.value
    }
}

public extension String {
    var htmlToAttributedString: NSAttributedString? {
        guard let data = data(using: .utf8) else { return nil }
        do {
            return try NSAttributedString(data: data,
                                          options: [NSAttributedString.DocumentReadingOptionKey.documentType:
                                                    NSAttributedString.DocumentType.html],
                                          documentAttributes: nil)
        } catch {
            return nil
        }
    }

    var htmlToString: String {
        return htmlToAttributedString?.string ?? ""
    }
}
