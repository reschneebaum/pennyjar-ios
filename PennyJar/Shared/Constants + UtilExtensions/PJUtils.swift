//
//  PJUtils.swift
//  PennyJar
//
//  Created by Rachel Schneebaum on 1/18/18.
//  Copyright © 2018 Rachel Schneebaum. All rights reserved.
//

import Foundation

public func delay(_ seconds: TimeInterval, completion: @escaping () -> Void) {
    DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + seconds,
                                   execute: completion)
}
