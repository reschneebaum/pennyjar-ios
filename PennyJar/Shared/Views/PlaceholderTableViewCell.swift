//
//  PlaceholderTableViewCell.swift
//  PennyJar
//
//  Created by Rachel Schneebaum on 1/23/18.
//  Copyright © 2018 Rachel Schneebaum. All rights reserved.
//

import UIKit

class PlaceholderTableViewCell: UITableViewCell {

    func configure(title: String) {
        textLabel?.text = title
    }
    
}
