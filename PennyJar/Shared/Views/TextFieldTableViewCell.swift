//
//  TextFieldTableViewCell.swift
//  PennyJar
//
//  Created by Rachel Schneebaum on 1/23/18.
//  Copyright © 2018 Rachel Schneebaum. All rights reserved.
//

import UIKit

protocol TextFieldTableViewCellDelegate: class {
    func textFieldTableViewCellShouldReturn(_ cell: TextFieldTableViewCell)
    func didSelectTextFieldTableViewCell(_ cell: TextFieldTableViewCell)
    func textFieldTableViewCellDidEndEditing(_ cell: TextFieldTableViewCell, finalText: String?)
}

class TextFieldTableViewCell: UITableViewCell {

    weak var delegate: TextFieldTableViewCellDelegate?

    @IBOutlet private weak var label: UILabel!
    @IBOutlet private weak var textField: UITextField! {
        didSet {
            textField.delegate = self
        }
    }

    func configure(placeholder: String, content: String?) {
        label.text = placeholder
        textField.placeholder = placeholder
        textField.text = content

        if placeholder.lowercased() == "password" {
            textField.isSecureTextEntry = true
            textField.returnKeyType = .go
        } else {
            textField.returnKeyType = .next
        }
    }
}

// MARK: - UITextFieldDelegate
extension TextFieldTableViewCell: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        delegate?.textFieldTableViewCellShouldReturn(self)
        return true
    }

    func textFieldDidBeginEditing(_ textField: UITextField) {
        delegate?.didSelectTextFieldTableViewCell(self)
    }

    func textFieldDidEndEditing(_ textField: UITextField) {
        textField.resignFirstResponder()
        delegate?.textFieldTableViewCellDidEndEditing(self, finalText: textField.text)
    }
}
