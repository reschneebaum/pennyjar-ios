//
//  DismissMenuAnimator.swift
//  PennyJar
//
//  Created by Rachel Schneebaum on 1/22/18.
//  Copyright © 2018 Rachel Schneebaum. All rights reserved.
//  menu animation: https://www.thorntech.com/2016/03/ios-tutorial-make-interactive-slide-menu-swift/
//

import UIKit

class DismissMenuAnimator: NSObject {}

extension DismissMenuAnimator: UIViewControllerAnimatedTransitioning {
    func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
        return 0.6
    }

    func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
        let containerView = transitionContext.containerView
        guard let fromVC = transitionContext.viewController(forKey: .from),
            let toVC = transitionContext.viewController(forKey: .to),
            let snapshot = containerView.viewWithTag(MenuService.snapshotNumber) else { return }

        UIView.animate(withDuration: transitionDuration(using: transitionContext), animations: {
            snapshot.frame = CGRect(origin: CGPoint.zero, size: UIScreen.main.bounds.size)
        }, completion: { _ in
            let didTransitionComplete = !transitionContext.transitionWasCancelled
            if didTransitionComplete {
                containerView.insertSubview(toVC.view, aboveSubview: fromVC.view)
                snapshot.removeFromSuperview()
            }
            transitionContext.completeTransition(didTransitionComplete)
        })
    }
}
