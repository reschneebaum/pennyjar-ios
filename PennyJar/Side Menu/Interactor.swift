//
//  Interactor.swift
//  PennyJar
//
//  Created by Rachel Schneebaum on 1/19/18.
//  Copyright © 2018 Rachel Schneebaum. All rights reserved.
//  menu animation: https://www.thorntech.com/2016/03/ios-tutorial-make-interactive-slide-menu-swift/
//

import UIKit

class Interactor: UIPercentDrivenInteractiveTransition {
    var hasStarted = false
    /// indicates whether the interaction should complete, or roll back
    var shouldFinish = false
}
