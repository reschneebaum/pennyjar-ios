//
//  MenuDelegate.swift
//  PennyJar
//
//  Created by Rachel Schneebaum on 1/22/18.
//  Copyright © 2018 Rachel Schneebaum. All rights reserved.
//

import UIKit

protocol MenuDelegate {
    func reopenMenu()
    func presentViewController(_ viewController: UIViewController.Type, sender: Any?)
    func presentStaticViewController(_ page: Page, sender: Any?)
}

extension MenuDelegate where Self: UIViewController {
    func reopenMenu() {
        performSegue(withIdentifier: IDs.Segue.menu, sender: nil)
    }

    func presentViewController(_ viewController: UIViewController.Type, sender: Any?) {
        dismiss(animated: true) {
            guard let destination = self.storyboard?.instantiateViewController(
                withIdentifier: viewController.id) else { return }
            self.navigationController?.show(destination, sender: sender)
        }
    }

    func presentStaticViewController(_ page: Page, sender: Any?) {
        dismiss(animated: true) {
            let destination = self.storyboard?.instantiateViewController(
                withIdentifier: StaticViewController.id) as! StaticViewController
            destination.page = page
            self.navigationController?.show(destination, sender: sender)
        }
    }
}

protocol Presenter {
    func presentViewController(_ viewController: UIViewController.Type, sender: Any?)
    func presentStaticViewController(_ page: Page, sender: Any?)
}

extension Presenter where Self: UIViewController {
    func presentViewController(_ viewController: UIViewController.Type, sender: Any?) {
        guard let destination = self.storyboard?.instantiateViewController(
            withIdentifier: viewController.id) else { return }
        self.navigationController?.show(destination, sender: sender)
    }

    func presentStaticViewController(_ page: Page, sender: Any?) {
        let destination = self.storyboard?.instantiateViewController(
            withIdentifier: StaticViewController.id) as! StaticViewController
        destination.page = page
        self.navigationController?.show(destination, sender: sender)
    }
}

protocol Logoutable {
    func logout()
}

extension Logoutable where Self: UIViewController {
    func logout() {
        AuthService.shared.logout { _ in
            _ = Keychain.removeValue(forKey: Keys.bearerToken)

            let loginVC = self.storyboard?.instantiateViewController(
                withIdentifier: LoginViewController.id) as! LoginViewController
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            appDelegate.switchRootViewController(loginVC)
        }
    }
}
