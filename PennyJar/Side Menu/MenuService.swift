//
//  MenuService.swift
//  PennyJar
//
//  Created by Rachel Schneebaum on 1/19/18.
//  Copyright © 2018 Rachel Schneebaum. All rights reserved.
//  menu animation: https://www.thorntech.com/2016/03/ios-tutorial-make-interactive-slide-menu-swift/
//

import UIKit

enum Direction {
    case up, down, left, right
}

struct MenuService {
    static let menuWidth: CGFloat = 0.8
    static let percentThreshold: CGFloat = 0.3
    static let snapshotNumber = 12345

    /// Calculates the progress in a particular direction: e.g., if you specify the `.right` direction,
    /// it only cares about movement along the positive-x direction; .left tracks progress in the negative-x direction.]
    ///
    /// - Parameters:
    ///   - translationInView: the user’s touch coordinates
    ///   - viewBounds: the screen’s dimensions
    ///   - direction: the direction that the slide-out menu is moving
    /// - Returns: the distance along the position specified
    static func calculateProgress(translationInView: CGPoint, viewBounds: CGRect, direction: Direction) -> CGFloat {
        let pointOnAxis: CGFloat
        let axisLength: CGFloat

        switch direction {
        case .up, .down:
            pointOnAxis = translationInView.y
            axisLength = viewBounds.height

        case .left, .right:
            pointOnAxis = translationInView.x
            axisLength = viewBounds.width
        }

        let movementOnAxis = pointOnAxis / axisLength
        let positiveMovementOnAxis: Float
        let positiveMovementOnAxisPercent: Float

        switch direction {
        case .right, .down: // positive
            positiveMovementOnAxis = fmaxf(Float(movementOnAxis), 0.0)
            positiveMovementOnAxisPercent = fminf(positiveMovementOnAxis, 1.0)
            return CGFloat(positiveMovementOnAxisPercent)

        case .left, .up:    // negative
            positiveMovementOnAxis = fminf(Float(movementOnAxis), 0.0)
            positiveMovementOnAxisPercent = fmaxf(positiveMovementOnAxis, -1.0)
            return CGFloat(-positiveMovementOnAxisPercent)
        }
    }

    /// This method maps the pan gesture state to various Interactor method calls.
    ///
    /// - Parameters:
    ///   - gestureState: the state of the pan gesture
    ///   - progress: how far across the screen the user has panned
    ///   - interactor: the UIPercentDrivenInteractiveTransition object that also serves as a state machine
    ///   - triggerSegue: a closure that is called to initiate the transition. The closure will contain something
    ///     like `performSegueWithIdentifier()`
    static func mapGestureStateToInteractor(gestureState: UIGestureRecognizerState, progress: CGFloat, interactor: Interactor?, triggerSegue: () -> Void) {
        guard let interactor = interactor else { return }
        switch gestureState {
        case .began:
            interactor.hasStarted = true
            triggerSegue()
        case .changed:
            interactor.shouldFinish = progress > percentThreshold
            interactor.update(progress)
        case .cancelled:
            interactor.hasStarted = false
            interactor.cancel()
        case .ended:
            interactor.hasStarted = false
            interactor.shouldFinish ? interactor.finish() : interactor.cancel()

        default: break
        }
    }
}
