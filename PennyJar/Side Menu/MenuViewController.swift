//
//  MenuViewController.swift
//  PennyJar
//
//  Created by Rachel Schneebaum on 1/19/18.
//  Copyright © 2018 Rachel Schneebaum. All rights reserved.
//  menu animation: https://www.thorntech.com/2016/03/ios-tutorial-make-interactive-slide-menu-swift/
//

import UIKit

private let cells: [String] = [
    LocalizedStrings.campaigns,
    LocalizedStrings.notifications,
    LocalizedStrings.settings,
    LocalizedStrings.privacy,
    LocalizedStrings.terms,
    LocalizedStrings.logout
]

class MenuViewController: UIViewController {

    var interactor: Interactor?
    var delegate: MenuDelegate?

    @IBOutlet private weak var tableView: UITableView!

    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.tableFooterView = UIView(frame: CGRect.zero)
    }

    func setupCell(for indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: IDs.TableViewCell.menu,
                                                 for: indexPath)
        cell.textLabel?.text = cells[indexPath.row]

        return cell
    }

    @IBAction func handleGesture(_ sender: UIPanGestureRecognizer) {
        let translation = sender.translation(in: view)
        let progress = MenuService.calculateProgress(translationInView: translation,
                                                     viewBounds: view.bounds,
                                                     direction: .left)
        MenuService.mapGestureStateToInteractor(gestureState: sender.state,
                                                progress: progress,
                                                interactor: interactor) {
            self.dismiss(animated: true, completion: nil)
        }
    }

    @IBAction func onCloseTapped(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }

}

// MARK: - UITableViewDataSource
extension MenuViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cells.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return setupCell(for: indexPath)
    }
}

// MARK: - UITableViewDelegate
extension MenuViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)

        switch indexPath.row {
        case 0:
            dismiss(animated: true, completion: nil)
        case 1:
            delegate?.presentViewController(NotificationsViewController.self, sender: self)
        case 2:
            delegate?.presentViewController(SettingsViewController.self, sender: self)
        case 3:
            delegate?.presentStaticViewController(.privacy, sender: self)
        case 4:
            delegate?.presentStaticViewController(.terms, sender: self)
        case 5:
            logout()
        default:
            return
        }
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
}

extension MenuViewController: Logoutable {}
